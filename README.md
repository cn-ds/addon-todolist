# TODO List

TODO List is an addon that can display a custom to do list for all the stuffs you HAVE/SHOULD/WANT to do.
It is very useful if you're playing multiples characters and need to follow if you have done your transmoruns or weekly chest.
The addon is customised with a simple list but you can disable and add more elements.

## Pictures

![Todo list](img/addon-pic1.png)

## How to use it

1. Click on the (I) to update the list
2. If you're satisfied click on "OK"
3. If you're not satisfied press "Escape" on your keyboard
4. Type /todo to display the command line interface help
5. Each new week you can use /todo reset to reset all the tasks

## Next step

1. Reduce the number of /reload needed to refresh the GUI
2. Make font size customizable
3. Automatically reset checkbox based on current date

## Disclaimer

The code is very low quality because I needed a quick'n dirty solution. If some people are downloading this addon I will update it.