local json = { _version = "0.1.2" }
DEFAULT_ELEMENTS = {
    {
        {"0101", "KO", "ENABLED", "Weekly mythic chest"},
        {"0102", "KO", "ENABLED", "Buy bonus roll #1"},
		{"0110", "KO", "ENABLED", "Buy bonus roll #2"},
		{"0111", "KO", "ENABLED", "Use bonus roll #1"},
		{"0112", "KO", "ENABLED", "Use bonus roll #2"},
        {"0103", "KO", "ENABLED", "NZoth nm or harder"},
		{"0113", "KO", "ENABLED", "World boss"},
        {"0104", "KO", "ENABLED", "Quest timewalker"},
        {"0105", "KO", "ENABLED", "Complete vision run"},
        {"0106", "KO", "ENABLED", "Weekly chest PVP"},
		{"0107", "KO", "ENABLED", "Assault Primary"},
		{"0108", "KO", "ENABLED", "Assault Secondary #1"},
		{"0109", "KO", "ENABLED", "Assault Secondary #2"},
        nil
    },
    nil
}

--DEFAULT_ELEMENTS = {
--    {
--        {"0101", "KO", "ENABLED", "Weekly mythic chest"},
--        {"0102", "KO", "ENABLED", "Buy bonus roll #1"},
--		{"0110", "KO", "ENABLED", "Buy bonus roll #2"},
--		{"0111", "KO", "ENABLED", "Use bonus roll #1"},
--		{"0112", "KO", "ENABLED", "Use bonus roll #2"},
--        {"0103", "KO", "ENABLED", "NZoth nm or harder"},
--        {"0104", "KO", "ENABLED", "Quest timewalker"},
--        {"0105", "KO", "ENABLED", "Complete vision run"},
--        {"0106", "KO", "ENABLED", "Weekly chest PVP"},
--		{"0107", "KO", "ENABLED", "Assault Primary"},
--		{"0108", "KO", "ENABLED", "Assault Secondary #1"},
--		{"0109", "KO", "ENABLED", "Assault Secondary #2"},
--        nil
--    },
--    {
--        {"0201", "KO", "ENABLED", "Daily corrompue"},
--        {"0202", "KO", "ENABLED", "Mini vision"},
--        nil
--    },
--    nil
--}

function EventHandler(self, event)
	if event == "ADDON_LOADED" then
		initVar()
		loadUI()
	end
	if event == "PLAYER_LOGOUT" or event == "PLAYER_LEAVING_WORLD" then
		ELEMENTS_SAVE = ELEMENTS
	end
end

function initVar()
	if (ELEMENTS_SAVE == nil) then
		--print("ELEMENTS WAS NULL")
		ELEMENTS = DEFAULT_ELEMENTS
	else
		--print("ELEMENTS WAS SET")
		ELEMENTS = ELEMENTS_SAVE
	end

end

SLASH_TODO1 = '/todo'
function SlashCmdList.TODO(msg, editbox)
    -- pattern matching that skips leading whitespace and whitespace between cmd and args
    -- any whitespace at end of args is retained
    local args = {}
    for substring in msg:gmatch("%S+") do
		table.insert(args, substring)
    end
    if args[1] == "show" then
        print (displayTodolistStatus())
    elseif args[1] == "add" then
        local size = table.getn(args)
        local itemName = ""
        local i = 4 --First part of the name of the item to add
        while i <= size do
            itemName = itemName .. " " .. args[i]
            i = i + 1
        end
        addNewItem(args[2], tonumber(args[3]), itemName)
    elseif args[1] == "remove" then

    elseif args[1] == "check" then
        findAndSet(args[2], "OK", 2)
    elseif args[1] == "uncheck" then
        findAndSet(args[2], "KO", 2)
    elseif args[1] == "disable" then
        findAndSet(args[2], "DISABLED", 3)
    elseif args[1] == "enable" then
        findAndSet(args[2], "ENABLED", 3)
    elseif args[1] == "reset" then
		resetAll()
		C_UI.Reload()
	elseif args[1] == "hardreset" then
		ELEMENTS = DEFAULT_ELEMENTS
		ELEMENTS_SAVE = DEFAULT_ELEMENTS
		C_UI.Reload()
    elseif args[1] == "gui" then
		loadUI()
    else
        print("Syntax: /todo show")
		print("Syntax: /todo gui")
        print("Syntax: /todo add [idToInsert] [Frequency] [name]")
        print("Syntax: /todo (remove | enable | disable | check | uncheck) [idToUpdate]")
        print("Syntax: /todo reset")
		print("Syntax: /todo hardreset")
    end
end

function addNewItem(id, frequency, name)
    table.insert(ELEMENTS[frequency], {id, "KO", "ENABLED", name})
end

function displayTodolistStatus ()
    local frequency = 1
    while ELEMENTS[frequency] do
        if (frequency == 1) then
            --print("------Weekly-------")
        elseif (frequency == 2) then
            --print("------Daily-------")
        end
        local elementId = 1
        while ELEMENTS[frequency][elementId] do
            if ELEMENTS[frequency][elementId][3] == "ENABLED" then
                --print(ELEMENTS[frequency][elementId][1]
				--	.. "-" .. ELEMENTS[frequency][elementId][2]
				--		.. "-" .. ELEMENTS[frequency][elementId][4])
            end
            elementId = elementId + 1
        end
        frequency = frequency + 1
    end
end

function resetAll ()
	local frequency = 1
    while ELEMENTS[frequency] do
        local elementId = 1
        while ELEMENTS[frequency][elementId] do
            ELEMENTS[frequency][elementId][2] = "KO"
            elementId = elementId + 1
        end
        frequency = frequency + 1
    end
end

function findAndSet (objectiveId, value, fieldNumber)
    local frequency = 1
    while ELEMENTS[frequency] do
        local elementId = 1
        while ELEMENTS[frequency][elementId] do
            if ELEMENTS[frequency][elementId][1] == objectiveId then
                ELEMENTS[frequency][elementId][fieldNumber] = value
                --print("Updating " .. objectiveId)
                break
            end
            elementId = elementId + 1
        end
        frequency = frequency + 1
    end
end

function findAndGet (objectiveId, fieldNumber)
    local frequency = 1
    while ELEMENTS[frequency] do
        local elementId = 1
        while ELEMENTS[frequency][elementId] do
            if ELEMENTS[frequency][elementId][1] == objectiveId then
                return ELEMENTS[frequency][elementId][fieldNumber]
            end
            elementId = elementId + 1
        end
        frequency = frequency + 1
    end
end

function openEditionFrame(self, button, down)
	local f = CreateFrame("Frame", "EditBox", UIParent, "DialogBoxFrame")
    f:SetPoint("CENTER")
    f:SetSize(600, 500)
    f:SetBackdrop({
        bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
        edgeFile = "Interface\\PVPFrame\\UI-Character-PVP-Highlight", -- this one is neat
        edgeSize = 16,
        insets = { left = 8, right = 6, top = 8, bottom = 8 },
    })
    f:SetBackdropBorderColor(0, .44, .87, 0.5) -- darkblue
        
    -- Movable
    f:SetMovable(true)
    f:SetClampedToScreen(true)
    f:SetScript("OnMouseDown", function(self, button)
        if button == "LeftButton" then
            self:StartMoving()
        end
    end)
    f:SetScript("OnMouseUp", f.StopMovingOrSizing)
        
    -- ScrollFrame
    local sf = CreateFrame("ScrollFrame", "EditBoxScrollFrame", EditBox, "UIPanelScrollFrameTemplate")
    sf:SetPoint("LEFT", 16, 0)
    sf:SetPoint("RIGHT", -32, 0)
    sf:SetPoint("TOP", 0, -16)
    sf:SetPoint("BOTTOM", EditBoxButton, "TOP", 0, 0)
        
    -- EditBox
    local eb = CreateFrame("EditBox", "EditBoxEditBox", EditBoxScrollFrame)
    eb:SetSize(sf:GetSize())
    eb:SetMultiLine(true)
    eb:SetAutoFocus(true)
    eb:SetFontObject("ChatFontNormal")
    eb:SetScript("OnEscapePressed", cancelUpdates)
	eb:SetText(json.encode(ELEMENTS):gsub("],","],\n"))
    sf:SetScrollChild(eb)
        
    -- Resizable
    f:SetResizable(true)
    f:SetMinResize(150, 100)
        
    local rb = CreateFrame("Button", "EditBoxResizeButton", EditBox)
    rb:SetPoint("BOTTOMRIGHT", -6, 7)
    rb:SetSize(16, 16)
        
    rb:SetNormalTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Up")
    rb:SetHighlightTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Highlight")
    rb:SetPushedTexture("Interface\\ChatFrame\\UI-ChatIM-SizeGrabber-Down")
        
    rb:SetScript("OnMouseDown", function(self, button)
        if button == "LeftButton" then
            f:StartSizing("BOTTOMRIGHT")
            self:GetHighlightTexture():Hide() -- more noticeable
        end
    end)
    rb:SetScript("OnMouseUp", function(self, button)
        f:StopMovingOrSizing()
        self:GetHighlightTexture():Show()
        eb:SetWidth(sf:GetWidth())
    end)
    f:Show()
	f:SetScript("OnHide", editFrameDissmiss)
end

function cancelUpdates(self)
	local editContent = json.encode(ELEMENTS):gsub("],","],\n")
	self:SetText(editContent)
	EditBox:Hide()
	EditBox = nil
end

function editFrameDissmiss(self)
    local updatedContent = EditBoxEditBox:GetText()
	local updatedArray = json.decode(updatedContent)
	ELEMENTS = updatedArray
	C_UI.Reload()
end

function loadUI ()
	MainFrame:SetBackdrop({
        bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
        edgeFile = "Interface\\PVPFrame\\UI-Character-PVP-Highlight", -- this one is neat
        edgeSize = 16,
        insets = { left = 8, right = 6, top = 8, bottom = 8 },
    })
    MainFrame:SetBackdropBorderColor(0, .44, .87, 0.5) -- darkblue

	local closeButton = CreateFrame("Button", "closeButton", MainFrame, "UIPanelCloseButton")
	closeButton:SetText("Close")
	closeButton:SetPoint("TOPRIGHT",0,0)
	closeButton:SetWidth(30)
	closeButton:SetHeight(30)
	closeButton:Enable()

	local editButton = CreateFrame("Button", "editButton", MainFrame, "UIPanelInfoButton")
	editButton:SetText("Edit")
	editButton:SetPoint("TOPRIGHT",-30,0)
	editButton:SetWidth(80)
	editButton:SetHeight(30)
	editButton:Enable()
	editButton:SetScript("OnClick", openEditionFrame)

	local frequency = 1
	local rowNum = 0
    while ELEMENTS[frequency] do
		addSeparatorLabel(rowNum, frequency)
		rowNum = rowNum + 1
        local elementId = 1
        while ELEMENTS[frequency][elementId] do
            if ELEMENTS[frequency][elementId][3] == "ENABLED" then
				local checked = false
				if (ELEMENTS[frequency][elementId][2] == "OK") then
					checked = true
				end
				addButtonAndCheckBox(rowNum, checked, ELEMENTS[frequency][elementId][4], ELEMENTS[frequency][elementId][1])
                --print(ELEMENTS[frequency][elementId][1]
				--	.. "-" .. ELEMENTS[frequency][elementId][2]
				--		.. "-" .. ELEMENTS[frequency][elementId][4])
				rowNum = rowNum + 1
            end
            elementId = elementId + 1
        end
        frequency = frequency + 1
    end
	MainFrame:SetHeight(30 + rowNum * 20)
end

function addSeparatorLabel(rowNum, frequency)
	local labelString = MainFrame:CreateFontString("text"..rowNum, "OVERLAY", "ChatFontNormal")
	labelString:SetPoint("TOPLEFT", 50, -20 - 20 * rowNum)--x, y
	if (frequency == 1) then
		labelString:SetText("Weekly")
	elseif (frequency == 2) then
		labelString:SetText("Daily")
	end
end

function addButtonAndCheckBox(rowNum, checked, name, id)
	local checkBox = CreateFrame("CheckButton", id, MainFrame, "UICheckButtonTemplate")
	checkBox:SetPoint("TOPLEFT", 10, -15 - 20 * rowNum)--x, y
	checkBox:SetWidth(20)
	checkBox:SetHeight(20)
	if (checked) then
		checkBox:SetChecked(checked)
	end
	checkBox:SetScript("OnClick", function(self, action)
		local buttonName = self:GetName()
		local currentValue = findAndGet(buttonName, 2)
		if(currentValue == "OK") then
			--print("Was checked")
			self:SetChecked(true)
			self:SetChecked(false)
			self:SetChecked(nil)
			findAndSet(buttonName, "KO", 2)
		else
			--print("Was not checked")
			self:SetChecked(true)
			findAndSet(buttonName, "OK", 2)
		end
		ELEMENTS_SAVE = ELEMENTS
	end)
	checkBox:Enable()

	local fontString = MainFrame:CreateFontString("text"..rowNum, "OVERLAY", "GameFontNormalSmall")
	fontString:SetPoint("TOPLEFT", 35, -20 - 20 * rowNum)--x, y
	fontString:SetText(name)
end


-- json.lua
--
-- Copyright (c) 2020 rxi
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy of
-- this software and associated documentation files (the "Software"), to deal in
-- the Software without restriction, including without limitation the rights to
-- use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do
-- so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--

-------------------------------------------------------------------------------
-- Encode
-------------------------------------------------------------------------------

local encode

local escape_char_map = {
  [ "\\" ] = "\\",
  [ "\"" ] = "\"",
  [ "\b" ] = "b",
  [ "\f" ] = "f",
  [ "\n" ] = "n",
  [ "\r" ] = "r",
  [ "\t" ] = "t",
}

local escape_char_map_inv = { [ "/" ] = "/" }
for k, v in pairs(escape_char_map) do
  escape_char_map_inv[v] = k
end


local function escape_char(c)
  return "\\" .. (escape_char_map[c] or string.format("u%04x", c:byte()))
end


local function encode_nil(val)
  return "null"
end


local function encode_table(val, stack)
  local res = {}
  stack = stack or {}

  -- Circular reference?
  if stack[val] then error("circular reference") end

  stack[val] = true

  if rawget(val, 1) ~= nil or next(val) == nil then
    -- Treat as array -- check keys are valid and it is not sparse
    local n = 0
    for k in pairs(val) do
      if type(k) ~= "number" then
        error("invalid table: mixed or invalid key types")
      end
      n = n + 1
    end
    if n ~= #val then
      error("invalid table: sparse array")
    end
    -- Encode
    for i, v in ipairs(val) do
      table.insert(res, encode(v, stack))
    end
    stack[val] = nil
    return "[" .. table.concat(res, ",") .. "]"

  else
    -- Treat as an object
    for k, v in pairs(val) do
      if type(k) ~= "string" then
        error("invalid table: mixed or invalid key types")
      end
      table.insert(res, encode(k, stack) .. ":" .. encode(v, stack))
    end
    stack[val] = nil
    return "{" .. table.concat(res, ",") .. "}"
  end
end


local function encode_string(val)
  return '"' .. val:gsub('[%z\1-\31\\"]', escape_char) .. '"'
end


local function encode_number(val)
  -- Check for NaN, -inf and inf
  if val ~= val or val <= -math.huge or val >= math.huge then
    error("unexpected number value '" .. tostring(val) .. "'")
  end
  return string.format("%.14g", val)
end


local type_func_map = {
  [ "nil"     ] = encode_nil,
  [ "table"   ] = encode_table,
  [ "string"  ] = encode_string,
  [ "number"  ] = encode_number,
  [ "boolean" ] = tostring,
}


encode = function(val, stack)
  local t = type(val)
  local f = type_func_map[t]
  if f then
    return f(val, stack)
  end
  error("unexpected type '" .. t .. "'")
end


function json.encode(val)
  return ( encode(val) )
end


-------------------------------------------------------------------------------
-- Decode
-------------------------------------------------------------------------------

local parse

local function create_set(...)
  local res = {}
  for i = 1, select("#", ...) do
    res[ select(i, ...) ] = true
  end
  return res
end

local space_chars   = create_set(" ", "\t", "\r", "\n")
local delim_chars   = create_set(" ", "\t", "\r", "\n", "]", "}", ",")
local escape_chars  = create_set("\\", "/", '"', "b", "f", "n", "r", "t", "u")
local literals      = create_set("true", "false", "null")

local literal_map = {
  [ "true"  ] = true,
  [ "false" ] = false,
  [ "null"  ] = nil,
}


local function next_char(str, idx, set, negate)
  for i = idx, #str do
    if set[str:sub(i, i)] ~= negate then
      return i
    end
  end
  return #str + 1
end


local function decode_error(str, idx, msg)
  local line_count = 1
  local col_count = 1
  for i = 1, idx - 1 do
    col_count = col_count + 1
    if str:sub(i, i) == "\n" then
      line_count = line_count + 1
      col_count = 1
    end
  end
  error( string.format("%s at line %d col %d", msg, line_count, col_count) )
end


local function codepoint_to_utf8(n)
  -- http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=iws-appendixa
  local f = math.floor
  if n <= 0x7f then
    return string.char(n)
  elseif n <= 0x7ff then
    return string.char(f(n / 64) + 192, n % 64 + 128)
  elseif n <= 0xffff then
    return string.char(f(n / 4096) + 224, f(n % 4096 / 64) + 128, n % 64 + 128)
  elseif n <= 0x10ffff then
    return string.char(f(n / 262144) + 240, f(n % 262144 / 4096) + 128,
                       f(n % 4096 / 64) + 128, n % 64 + 128)
  end
  error( string.format("invalid unicode codepoint '%x'", n) )
end


local function parse_unicode_escape(s)
  local n1 = tonumber( s:sub(1, 4),  16 )
  local n2 = tonumber( s:sub(7, 10), 16 )
   -- Surrogate pair?
  if n2 then
    return codepoint_to_utf8((n1 - 0xd800) * 0x400 + (n2 - 0xdc00) + 0x10000)
  else
    return codepoint_to_utf8(n1)
  end
end


local function parse_string(str, i)
  local res = ""
  local j = i + 1
  local k = j

  while j <= #str do
    local x = str:byte(j)

    if x < 32 then
      decode_error(str, j, "control character in string")

    elseif x == 92 then -- `\`: Escape
      res = res .. str:sub(k, j - 1)
      j = j + 1
      local c = str:sub(j, j)
      if c == "u" then
        local hex = str:match("^[dD][89aAbB]%x%x\\u%x%x%x%x", j + 1)
                 or str:match("^%x%x%x%x", j + 1)
                 or decode_error(str, j - 1, "invalid unicode escape in string")
        res = res .. parse_unicode_escape(hex)
        j = j + #hex
      else
        if not escape_chars[c] then
          decode_error(str, j - 1, "invalid escape char '" .. c .. "' in string")
        end
        res = res .. escape_char_map_inv[c]
      end
      k = j + 1

    elseif x == 34 then -- `"`: End of string
      res = res .. str:sub(k, j - 1)
      return res, j + 1
    end

    j = j + 1
  end

  decode_error(str, i, "expected closing quote for string")
end


local function parse_number(str, i)
  local x = next_char(str, i, delim_chars)
  local s = str:sub(i, x - 1)
  local n = tonumber(s)
  if not n then
    decode_error(str, i, "invalid number '" .. s .. "'")
  end
  return n, x
end


local function parse_literal(str, i)
  local x = next_char(str, i, delim_chars)
  local word = str:sub(i, x - 1)
  if not literals[word] then
    decode_error(str, i, "invalid literal '" .. word .. "'")
  end
  return literal_map[word], x
end


local function parse_array(str, i)
  local res = {}
  local n = 1
  i = i + 1
  while 1 do
    local x
    i = next_char(str, i, space_chars, true)
    -- Empty / end of array?
    if str:sub(i, i) == "]" then
      i = i + 1
      break
    end
    -- Read token
    x, i = parse(str, i)
    res[n] = x
    n = n + 1
    -- Next token
    i = next_char(str, i, space_chars, true)
    local chr = str:sub(i, i)
    i = i + 1
    if chr == "]" then break end
    if chr ~= "," then decode_error(str, i, "expected ']' or ','") end
  end
  return res, i
end


local function parse_object(str, i)
  local res = {}
  i = i + 1
  while 1 do
    local key, val
    i = next_char(str, i, space_chars, true)
    -- Empty / end of object?
    if str:sub(i, i) == "}" then
      i = i + 1
      break
    end
    -- Read key
    if str:sub(i, i) ~= '"' then
      decode_error(str, i, "expected string for key")
    end
    key, i = parse(str, i)
    -- Read ':' delimiter
    i = next_char(str, i, space_chars, true)
    if str:sub(i, i) ~= ":" then
      decode_error(str, i, "expected ':' after key")
    end
    i = next_char(str, i + 1, space_chars, true)
    -- Read value
    val, i = parse(str, i)
    -- Set
    res[key] = val
    -- Next token
    i = next_char(str, i, space_chars, true)
    local chr = str:sub(i, i)
    i = i + 1
    if chr == "}" then break end
    if chr ~= "," then decode_error(str, i, "expected '}' or ','") end
  end
  return res, i
end


local char_func_map = {
  [ '"' ] = parse_string,
  [ "0" ] = parse_number,
  [ "1" ] = parse_number,
  [ "2" ] = parse_number,
  [ "3" ] = parse_number,
  [ "4" ] = parse_number,
  [ "5" ] = parse_number,
  [ "6" ] = parse_number,
  [ "7" ] = parse_number,
  [ "8" ] = parse_number,
  [ "9" ] = parse_number,
  [ "-" ] = parse_number,
  [ "t" ] = parse_literal,
  [ "f" ] = parse_literal,
  [ "n" ] = parse_literal,
  [ "[" ] = parse_array,
  [ "{" ] = parse_object,
}


parse = function(str, idx)
  local chr = str:sub(idx, idx)
  local f = char_func_map[chr]
  if f then
    return f(str, idx)
  end
  decode_error(str, idx, "unexpected character '" .. chr .. "'")
end


function json.decode(str)
  if type(str) ~= "string" then
    error("expected argument of type string, got " .. type(str))
  end
  local res, idx = parse(str, next_char(str, 1, space_chars, true))
  idx = next_char(str, idx, space_chars, true)
  if idx <= #str then
    decode_error(str, idx, "trailing garbage")
  end
  return res
end